// parameters
let p = {
  boundBox: true,
}

// the model
let handpose;
// latest model predictions
let predictions = [];
// video capture
let video;

let mic;
let fft;

function setup() {
  createCanvas(640, 480);
  background('#000000')
  video = createCapture(VIDEO);
  video.size(width, height);

  // add params to a GUI
  createParamGui(p, paramChanged);

  // initialize the model
  handpose = ml5.handpose(video, modelReady);

  // This sets up an event that fills the global variable "predictions"
  // with an array every time new predictions are made
  handpose.on("predict", results => {
    predictions = results;
  });

  // Hide the video element, and just show the canvas
  video.hide();

  mic = new p5.AudioIn();
  mic.start();

  fft = new p5.FFT();
  fft.setInput(mic);
}

function modelReady() {
  console.log("Model ready!");
}

function draw() {
  push();
  scale(-1,1);
  translate(-width, 0);
  //image(video, 0, 0, width, height);
  pop();
  
  // different visualizations
  if (p.boundBox) drawBoundingBoxes()

  let waveform = fft.waveform();
    for (let i = 0; i < waveform.length; i++) {
      if (waveform[i] === 1) {
      background('#000000')
      }
    }
}

// draw bounding boxes around all detected hands
function drawBoundingBoxes() {

  for (let p of predictions) {
    const bb = p.boundingBox
    // get bb coordinates
    const x = width - bb.bottomRight[0]
    const y = bb.topLeft[1]
    const w = bb.bottomRight[0] - bb.topLeft[0]
    const h = bb.bottomRight[1] - bb.topLeft[1]

    let handX = x + w/2;
    let handY = y + h/2;
    
    noFill();
    stroke('White');
    let waveform = fft.waveform();
    for (let i = 0; i < waveform.length; i++) {
    ellipse(handX, handY, waveform[i] * 500, waveform[i] * 500);
    }
  }
}

// global callback from the settings GUI
function paramChanged(name) {
}